'''
This script encapsulate a class to extract related comments, and complement sentiment tag
and postcode tags for comments.
'''

import pandas as pd
import nltk
import nltk.data
from gensim.utils import simple_preprocess
from nltk.stem import WordNetLemmatizer, SnowballStemmer
from nltk.corpus import stopwords
import csv
from gensim.models.doc2vec import Doc2Vec
from joblib import dump, load
from SentimentAnalyzer import SentimentAnalyzer

import matplotlib.path as mlpath
import shapefile
from pyproj import Transformer
from shapely.geometry import shape, mapping, Point, Polygon, MultiPolygon

myshp = open("data/england_pcd_2012.shp", 'rb')
mydbf = open("data/england_pcd_2012.dbf", 'rb')
ploys = shapefile.Reader(shp=myshp, dbf=mydbf)

shapeRecords = ploys.shapeRecords()

def checkPostcode(Longitude, Latitude):
    transformer = Transformer.from_crs(4326, 27700, always_xy=True)
    x1, y1 = Longitude, Latitude
    x2, y2 = transformer.transform(x1, y1)
    postcode = 'unknown'

    p = Point(x2, y2)
    for shapeRecord in shapeRecords:
        ploy = shape(shapeRecord.shape.__geo_interface__)
        if p.within(ploy):
            postcode = shapeRecord.record[1]
            break

    return postcode
  
checkPostcode(-0.210343, 51.524752)

class Extracter:
    
    def __init__(self):
        self.kmeans = load('/gdrive/My Drive/Model/KMeans.joblib')
        self.doc2vec = Doc2Vec.load('/gdrive/My Drive/Model/Doc2Vec1')
        self.sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')

    def read_data(self, start_number, end_number):
        '''
        read csv files from start file to end file
        '''
        dataframes = [pd.read_csv('/gdrive/My Drive/Cleaned/Review%s.csv' % str(i+1)) for i in range(start_number, end_number)]
        dataframe = pd.concat(dataframes, ignore_index = True)
        return dataframe
    
    def filter_out(self, review):
        '''
        filter out unrelated comments in the reviews
        '''
        sentences = self.sent_detector.tokenize(review.strip())
        result = []
        for sent in sentences:
            if self.predict(sent):
                result.append(sent)
        return ''.join(result)

    def predict(self, sent):
        '''
        determine whether a sentence is about food environment
        '''
        X=[self.doc2vec.infer_vector(self.preprocess(sent), epochs=60)]
        labels = self.kmeans.predict(X)
        #print(labels)
        if labels[0] == 12: # 12 is the cluster number for food environment
            return 1
        else:
            return 0

    def preprocess(self, text):
        '''
        preprocess raw data: tokenization, remove stopwords, and lemmatization
        '''
        tokens = simple_preprocess(text)
    
        stop_words = stopwords.words('english')
        stop_words.extend(['london', 'leeds', 'u', 'would', 'could', 'us'])
    
        tokens = [SnowballStemmer('english').stem(token) for token in tokens if token not in stop_words]
        return tokens

    def modify(self, dataframe):
        cols = []
        for i,r in dataframe.iterrows():
            new_string = self. filter_out(r[4])
            dataframe.loc[i, "Review Text"] = new_string
            if not new_string:
                cols.append(i)
        
        dataframe = dataframe.drop(cols)
        return dataframe

    def save_data(self, dataframe, filename):
        dataframe.to_csv(filename, quoting = csv.QUOTE_ALL, index = False)

    def predict_sentiment(self, dataframe):
        dataframe.loc['Sentiment'] = 1
        analyzer = SentimentAnalyzer()

        for i,r in dataframe.iterrows():
            dataframe.loc[i, 'Sentiment'] = analyzer.sentiment(str(r[4]))
        
        return dataframe

    def modifyPost(self, dataframe):
        dataframe['postcode'] = 'unknown'
        postcode = None
        propertyID = None
        for i,r in dataframe.iterrows():
            try:
                if propertyID == str(r[1]):
                    dataframe.loc[i, 'postcode'] = postcode 
                else:
                    postcode=checkPostcode(float(r[3]), float(r[2]))
                    dataframe.loc[i, 'postcode'] = postcode
                    propertyID = str(r[1])
            except:
                pass
        
        return dataframe
  
    def process_dataframe(self, start_number, end_number):
        dataframe = self.read_data(start_number, end_number)
        dataframe = self.modify(dataframe)
        dataframe = self.predict_sentiment(dataframe)
        dataframe = self.modifyPost(dataframe)
        self.save_data(dataframe, '/gdrive/My Drive/Extracted/Review%s.csv' % str(start_number+1))