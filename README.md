# AirBnB

The Characterization and visualization of the food environment in London based on Airbnb reviews. 

### Notebook Files:
1. Data Profiling, display the procedures to deeply analyze the textual data
2. Data Cleaning, display the procedures to clean raw data
3. Data preprocess, display the procedures to preprocess data in order to make it applicable for models
4. Keyword-based approach, display the construction and evaluation of keyword-based classifier
5. Clustering-based approach, display the construction and evaluation of clustering-based classifier
6. LDA-based approach, display the construction and evaluation of LDA-based classifier
7. Sentiment analysis comparisons, display the evaluation of different methods to predict the sentiment of texts

### Python Script Files:
1. Sentiment Analyzer, encapsulate the lexicon-based sentiment analyzer
2. Processor, encapsulate the processor that transform data to suitable form for visualization

### Data File:
1. Lexicon file for lexicon-based sentiment analysis
2. Product sentiment dataset
3. Movie sentiment dataset
4. Tweet sentiment dataset