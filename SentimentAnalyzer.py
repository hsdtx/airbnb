'''
As stated in the dissertation, the sentiment analyser based on a lexicon to calculate the sentiment score
of texts. Meanwhile, it also exploits negation rules and intensity rules to tune the sentiment score.
'''

import math
import spacy

B_INCR = 1.5
B_DECR = 0.5

NEGATE = \
    ["aint", "arent", "cannot", "cant", "couldnt", "darent", "didnt", "doesnt",
     "ain't", "aren't", "can't", "couldn't", "daren't", "didn't", "doesn't",
     "dont", "hadnt", "hasnt", "havent", "isnt", "mightnt", "mustnt", "neither",
     "don't", "hadn't", "hasn't", "haven't", "isn't", "mightn't", "mustn't",
     "neednt", "needn't", "never", "none", "nope", "nor", "not", "nothing", "nowhere",
     "oughtnt", "shant", "shouldnt", "uhuh", "wasnt", "werent",
     "oughtn't", "shan't", "shouldn't", "uh-uh", "wasn't", "weren't",
     "without", "wont", "wouldnt", "won't", "wouldn't", "rarely", "seldom", "despite"]

BOOSTER_DICT = \
    {"absolutely": B_INCR, "amazingly": B_INCR, "awfully": B_INCR, 
     "completely": B_INCR, "considerable": B_INCR, "considerably": B_INCR,
     "decidedly": B_INCR, "deeply": B_INCR, "effing": B_INCR, "enormous": B_INCR, "enormously": B_INCR,
     "entirely": B_INCR, "especially": B_INCR, "exceptional": B_INCR, "exceptionally": B_INCR, 
     "extreme": B_INCR, "extremely": B_INCR,
     "fabulously": B_INCR, "flipping": B_INCR, "flippin": B_INCR, "frackin": B_INCR, "fracking": B_INCR,
     "fricking": B_INCR, "frickin": B_INCR, "frigging": B_INCR, "friggin": B_INCR, "fully": B_INCR, 
     "fuckin": B_INCR, "fucking": B_INCR, "fuggin": B_INCR, "fugging": B_INCR,
     "greatly": B_INCR, "hella": B_INCR, "highly": B_INCR, "hugely": B_INCR, 
     "incredible": B_INCR, "incredibly": B_INCR, "intensely": B_INCR, 
     "major": B_INCR, "majorly": B_INCR, "more": B_INCR, "most": B_INCR, "particularly": B_INCR,
     "purely": B_INCR, "quite": B_INCR, "really": B_INCR, "remarkably": B_INCR,
     "so": B_INCR, "substantially": B_INCR,
     "thoroughly": B_INCR, "total": B_INCR, "totally": B_INCR, "tremendous": B_INCR, "tremendously": B_INCR,
     "uber": B_INCR, "unbelievably": B_INCR, "unusually": B_INCR, "utter": B_INCR, "utterly": B_INCR,
     "very": B_INCR,
     "almost": B_DECR, "barely": B_DECR, "hardly": B_DECR, "just enough": B_DECR,
     "kind of": B_DECR, "kinda": B_DECR, "kindof": B_DECR, "kind-of": B_DECR,
     "less": B_DECR, "little": B_DECR, "marginal": B_DECR, "marginally": B_DECR,
     "occasional": B_DECR, "occasionally": B_DECR, "partly": B_DECR,
     "scarce": B_DECR, "scarcely": B_DECR, "slight": B_DECR, "slightly": B_DECR, "somewhat": B_DECR,
     "sort of": B_DECR, "sorta": B_DECR, "sortof": B_DECR, "sort-of": B_DECR}

class SentimentAnalyzer(object):

    def __init__(self):
        self._dict = self.make_lex_dict()

    def make_lex_dict(self):
        """
        Convert lexicon file to a dictionary
        """
        file = open('data/vader_lexicon.txt')
        lex_dict = {}
        for line in file.readlines():
            if not line:
                continue
            (word, measure) = line.strip().split('\t')[0:2]
            lex_dict[word] = float(measure)
        return lex_dict

    def normalize(self, score, alpha=15):
        """
        Normalize the score to be between -1 and 1 using an alpha that
        approximates the max expected value
        """
        norm_score = score / math.sqrt((score * score) + alpha)
        if norm_score < -1.0:
            return -1.0
        elif norm_score > 1.0:
            return 1.0
        else:
            return norm_score

    def sentiment_score(self, text):
        nlp = spacy.load("en_core_web_sm")
        doc = nlp(text)
        sentiments = []
        for i, token in enumerate(doc):
            if token.pos_ in ['NOUN', 'VERB', 'ADJ', 'ADV']:
                individual_score = None
                try:
                    individual_score = self._dict[token.lemma_]
                except:
                    pass
            
                if individual_score != None:
                    deps = [child.dep_ for child in token.children]
                
                    for child in token.children:
                        if 'mod' in child.dep_ :
                            if child.lemma_ in BOOSTER_DICT:
                                individual_score *= BOOSTER_DICT[child.lemma_]
                    
                    if 'neg' in deps or doc[i-1].lemma_ in NEGATE or doc[i-2].lemma_ in NEGATE:
                        if individual_score > 0:
                            individual_score -= 3
                        else:
                            individual_score += 3
                    sentiments.append(individual_score)

        return self.normalize(sum(sentiments))

    def sentiment(self, text):
        score = self.sentiment_score(text)
            
        if score > 0.1:
            return 2
        elif score <= 0.1 and score >= -0.1:
            return 1
        else:
            return 0

s = SentimentAnalyzer()
score = s.sentiment("while i would like to give it 3.5 stars instead of 4, i gave it a 4 because it has run well for me during the time i have owned it.")
print(score)